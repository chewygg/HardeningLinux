# HardeningLinux
Registro de Estudos

# Reconhecimento
Realizar a verificação do que está vulnerável, mapeamento das ameaças e mitigação dos riscos

## SO

### Verificar boot loader
```
grub-probe -t device /boot/grub
ls -l /etc/grub.d/
cat /etc/grub.d/01_users 
cat /etc/grub.d/00_headers
cat /etc/grub.d/10_linux
```

### Users
```
# Listando
getent passwd
getent group
passwd -Sa
# Verificando umask
cat /etc/user/.bashrc | grep umask
cat /etc/skel/.bashrc | grep umask
chage -l USER
awk -F: '($2 == "") {print}' /etc/shadow #Contas sem senha
awk -F: '($3 == "0") {print}' /etc/passwd #Contas não root com ID de root
```

### Serviços
```
runlevel
chkconfig --list
chkconfig --list | grep -F '3:on'
chkconfig --del <serviço>
chkconfig --status-all
systemctl list-units --type service
systemctl list-units --type service --state running
systemctl list-units --type service --state running | grep ctrl*
lsof -i -n | egrep 'COMMAND|LISTEN|UDP'

sudo systemctl status atd
sudo systemctl stop atd
sudo systemctl disable atd
sudo systemctl mask atd
sudo systemctl mask ctrl+alt_del.target
```

### Portas
```
netstat -tulp #Programas com portas TCP e UDP que estao escutando  
nmap -sTU REMOTE_HOST
ss -nltp
lsof -i -n | egrep 'COMMAND|LISTEN|UDP'
```

### Listar arquivos abertos
```
lsof PATH_TO_FILE #qual processo abriu o arquivo
lsof -D PATH_TO_FILE #qual processo abriu o arquivo dentro do dir
lsof -c PROCESSO #Arq abertos pelo processo
lsof -u USER #lista files abertos de um user especifico
lsof -g GID #arquivos abertos por determinado grupo
lsof -p PID #arquivos abertos por determinado processo
lsof -i #conexões abertas

lsof -i -n #todas as conexões de rede abertas 
lsof -i -n | egrep 'COMMAND|LISTEN|UDP' #lista arquivos abertos com cnx ativas
lsof /bin/bash #verificar quem esta usando o /bin/bash

```

### Verificar pacotes
```
rpm -qa
rpm -qi #detalhes sobre o PACKAGE_NAME
rpm -e --test PACKAGE_NAME #dependencias do pacote e potenciais conflitos
# no ubuntu
dpkg -s PACKAGE_NAME #status do pkg instalado
dpkg -I PACKAGE_NAME do pacote #info do pkg instalado
apt list --installed #lista pkg instalados
apt-cache showpkg PACKAGE_NAME #info sobre o pkg
apt-get -u upgrade #List upgrades sem instalar
apt-get install PACKAGE_NAME--no-upgrade # instala pacote sem upgrade
```
### SSHD
`/etc/ssh/sshd_config`

### NTP
```
/etc/ntp.conf 
ntpq -c rv 200.201.216.158
```
