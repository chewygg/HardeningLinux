
#!/bin/bash

sleep 2 && clear

##################################################################################################################
echo -e "=====================================================================" | tee --append $1.txt
echo -e "1.0 Verificação de rede " | tee --append $1.txt
echo ""
echo "Comando usado: hostname -I " | tee --append $1.txt
echo -e "1.1 - Interfaces pelas quais o host responde: "$(hostname -I | tee --append $1.txt)""
sleep 2
echo ""
echo -e "Comando usado: hostname -f " | tee --append $1.txt
echo -e "1.2 - FQDN: "$(hostname -f | tee --append $1.txt)""
sleep 2
echo ""
echo -e "Comando usado: ip -br a" | tee --append $1.txt
echo -e "1.3 - Informações sobre as NICs do server: \n "$(ip -br a | tee --append $1.txt)""
sleep 7 && clear

##################################################################################################################
echo "===================================================================== " | tee --append $1.txt
echo -e "2.0 - Verificando SO " | tee --append $1.txt
echo ""
echo -e "Comando usado: cat /etc/os-release | grep PRETTY_NAME " | tee --append $1.txt
echo -e "2.1 - Versão do sistema operacional "$(cat /etc/os-release | grep PRETTY_NAME |cut -d = -f 2 | tee --append $1.txt)""
echo ""
sleep 7 && clear

##################################################################################################################
echo -e "=====================================================================" | tee --append $1.txt
echo -e "3.0 - Verificando Users " | tee --append $1.txt
echo ""
sleep 2
echo -e "3.1 - Usuários com UID maior que 1000: \n "$(awk -F: '$3 > 999 {print $0}' /etc/passwd)""
awk -F: '$3 > 999 {print $0}' /etc/passwd >> $1.txt
echo ""
sleep 2
echo -e "3.2 - Contas não root com ID de root \n "$(awk -F: '($3 == "0") {print}' /etc/passwd | tee --append $1.txt)""
awk -F: '($3 == "0") {print}' /etc/passwd >> $1.txt
echo ""
echo -e "3.3 - Verificando sudo: \n "$(sudo cat /etc/sudoers | grep -Ev '^#|^$')""
sudo cat /etc/sudoers | grep -Ev '^#|^$' >> $1.txt

sleep 7 && clear
##################################################################################################################
echo -e "===================================================================== " | tee --append $1.txt
echo -e "4.0 - Verificando Serviços" | tee --append $1.txt

echo ""
echo -e "Comando usado: systemctl list-units --type service --state running"
sleep 2
echo -e "4.1 - Verificando serviços running: "$(systemctl list-units --type service --state running)""
systemctl list-units --type service --state running >> $1.txt
echo ""
echo -e "Comando usado: systemctl list-unit-files | grep ctrl* "
echo -e "4.2 - Verificando serviços running: "$(systemctl list-unit-files | grep ctrl*)""
systemctl list-unit-files | grep ctrl* >> $1.txt

sleep 7
clear
echo -e "Done!"
sleep 2
